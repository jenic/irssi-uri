# URI

*URI Title Fetching (IRSSI/WeeChat)*

# SYNOPSIS

**uri** fetches the title of a URL posted and prints it in WeeChat.

**IRSSI Script (uri.pl) is no longer actively developed as I use WeeChat now.**

**wee-uri.pl requires cURL**

Features include:

* Optional window to store uri's and their titles
* Caching to avoid bandwidth waste
* PCRE-based blacklist for URI's
* Ability to ignore specific buffers

# OPTIONS

- **debug** 0 - 3

    Print debug messages of various verbosity from 0 (none) to 3 (noisy)
    
    Default: 0

- **xown** 1 | 0

    Act on own urls
    
    Default: 0

- **single_nick** str | 0

    Skip call to find own nickname (hardcode your nick)
    
    Default: 0
    
    Example: jenic

- **cache** int

    Number of urls to cache
    
    Default: 5

- **cachet** int

    Time (in sec) to keep cached entries
    
    Default: 3600

- **blfile** str

    Full path to blacklist file
    
    Default: ~/.weechat/.uribl

- **mode** 0 - 2

    * 0: Print in current buffer
    * 1: Print in dedicated buffer
    * 2: Both 0 and 1
    
    Default: 0

- **window** str

    Name of buffer to use for modes > 0
    
    Default: uri

- **maxdl** int

    Max limit on download size, in bytes
    
    Default: 1e6

- **timeout** int

    Child execution time, in milliseconds
    
    Default: 9001

- **ignore** str

    Buffers to ignore, comma (,) delimited string
    
    Default: NULL
    
    Example: ##news,#HorribleSubs

- **ua** str

    Curl's declared user agent
    
    Default: Mozilla/4.0

- **clim** int

    Enforce Title character limit
    
    Default: 120

- **mime** str

    Acceptable MIME types
    
    Default: */*
    
    Example: text/html,*/*

# NOTE on WAFs and UA

Certain unenlightened websites will block cURL's default UA and by extension,
this URI script. Typically I use the following setting in WeeChat to get around
this:

    /set plugins.var.perl.uri.ua Mozilla/5.0 (Windows NT 5.1; rv:2.0) Gecko/20100101 Firefox/4.0

Some WAFs (Web Application Firewall) use the header order to identify browser
types. This author uses Firefox and the script was modeled after the header
order of Firefox requests. If you should decide to make your user agent
something other than the default or Firefox, note that your mileage may vary.

# CREDITS

This script is originally based on the fork of Toni Viemerö's spotifyuri
script, by Caesar Ahlenhed for irssi. Their websites are below:

http://spotify.url.fi/

http://sniker.codebase.nu/